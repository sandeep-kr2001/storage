import connexion
from connexion import NoContent
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from base import Base
from deposit import Deposit  
from withdrawal import Withdrawal  
import datetime
import yaml
import logging.config
import logging
from pykafka import KafkaClient
from threading import Thread 
from pykafka.common import OffsetType
import json

with open('log_conf.yml', 'r') as f:
    log_config = yaml.safe_load(f.read())
    logging.config.dictConfig(log_config)
    
logger = logging.getLogger('basicLogger')

with open('app_conf.yaml', 'r') as f:
    app_config = yaml.safe_load(f.read())


user = app_config['datastore']['user']
password = app_config['datastore']['password']
hostname = app_config['datastore']['hostname']
port = app_config['datastore']['port']
db = app_config['datastore']['db']

logger.info(f"Connecting to MySQL database at {hostname}:{port}:{db}")

create_engine_str = f'mysql+pymysql://{user}:{password}@{hostname}:{port}/{db}'

DB_ENGINE = create_engine(create_engine_str)
DB_SESSION = sessionmaker(bind=DB_ENGINE)


def report_deposit(body):
    pass
    # """ Receives a Deposit reading """
    # session = DB_SESSION()


    # dp = Deposit(
    #     body['account_id'],
    #     body['deposit_date'],
    #     body['deposit_amount'],
    #     body['account_balance'],
    #     body['trace_id']
    # )


    # session.add(dp)
    # session.commit()
    # session.close()
    # logger.debug("Stored event - deposit request with a trace id of %s",body["trace_id"])
    # return NoContent, 201

def get_deposit_readings(timestamp):
    """ Gets new deposit readings after the timestamp """
    session = DB_SESSION()
    logger.info(f"Received timestamp: {timestamp}")
    timestamp_datetime = datetime.datetime.strptime(timestamp, "%Y-%m-%dT%H:%M:%SZ")


    readings = session.query(Deposit).filter(Deposit.date_created >=timestamp_datetime)
    results_list = []
    for reading in readings:
        results_list.append(reading.to_dict())
    session.close()
    logger.info("Query for Deposit readings after %s returns %d results" %(timestamp, len(results_list)))
    return results_list, 200

def report_withdrawal(body):
    pass
    # """ Receives a Withdrawal reading """
    # session = DB_SESSION()


    # wl = Withdrawal(
    #     body['account_id'],
    #     body['fee'],
    #     body['withdrawal_amount'],
    #     body['withdrawal_date'],
    #     body['trace_id'],

    # )
    # session.add(wl)
    # session.commit()
    # session.close()
    # logger.debug("Stored event - withdrawal request with a trace id of %s",body["trace_id"])
    # return NoContent, 201

def get_withdrawal_readings(timestamp):
    """ Gets new withdrawal readings after the timestamp """
    session = DB_SESSION()
    logger.info(f"Received timestamp: {timestamp}")
    timestamp_datetime = datetime.datetime.strptime(timestamp, "%Y-%m-%dT%H:%M:%SZ")


    readings = session.query(Withdrawal).filter(Withdrawal.date_created >=timestamp_datetime)
    results_list = []
    for reading in readings:
        results_list.append(reading.to_dict())
    session.close()
    logger.info("Query for Withdrawal readings after %s returns %d results" %(timestamp, len(results_list)))
    return results_list, 200

def process_messages():

    hostname = "%s:%d" % (app_config["events"]["hostname"],
    app_config["events"]["port"])
    client = KafkaClient(hosts=hostname)
    topic = client.topics[str.encode(app_config["events"]["topic"])]
    consumer = topic.get_simple_consumer(consumer_group=b'event_group',
    reset_offset_on_start=False,
    auto_offset_reset=OffsetType.LATEST)
    for msg in consumer:
        msg_str = msg.value.decode('utf-8')
        msg = json.loads(msg_str)
        logger.info("Message: %s" % msg)
        payload = msg["payload"]
        # print(msg['type'])
        
        if msg["type"] == "deposit": # Change this to your event type
        # Store the event1 (i.e., the payload) to the DB
            session = DB_SESSION()

            dp = Deposit(
                payload['account_id'],
                payload['deposit_date'],
                payload['deposit_amount'],
                payload['account_balance'],
                payload['trace_id']
        )

            session.add(dp)

            session.commit()
            session.close()
           

        elif msg["type"] == "withdrawal": # Change this to your event type
            session = DB_SESSION()

            wl = Withdrawal(
                payload['account_id'],
                payload['fee'],
                payload['withdrawal_amount'],
                payload['withdrawal_date'],
                payload['trace_id'],

            )

            session.add(wl)
            session.commit()
            session.close()
        # Store the event2 (i.e., the payload) to the DB
        # Commit the new message as being read
        consumer.commit_offsets()


app = connexion.FlaskApp(__name__, specification_dir='')
app.add_api("openapi.yaml", strict_validation=True, validate_responses=True)

if __name__ == "__main__":
    t1 = Thread(target=process_messages)
    t1.setDaemon(True)
    t1.start()

    app.run(port=8090)
