import sqlite3

conn = sqlite3.connect('finance.sqlite')

c = conn.cursor()

c.execute('''
    CREATE TABLE IF NOT EXISTS Withdrawal
    (id INTEGER PRIMARY KEY ASC, 
     account_id REAL NOT NULL,
     fee REAL NOT NULL,
     withdrawal_amount REAL NOT NULL,
     withdrawal_date VARCHAR(100) NOT NULL,
     date_created VARCHAR(100) NOT NULL)
''')

c.execute('''
    CREATE TABLE IF NOT EXISTS Deposit
    (id INTEGER PRIMARY KEY ASC, 
     account_id REAL NOT NULL,
     deposit_amount REAL NOT NULL,
     account_balance REAL NOT NULL,
     deposit_date VARCHAR(100) NOT NULL,
     date_created VARCHAR(100) NOT NULL)
''')

conn.commit()
conn.close()
