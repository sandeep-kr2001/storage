import sqlite3

conn = sqlite3.connect('finance.sqlite')

c = conn.cursor()
c.execute('''
          DROP TABLE Withdrawal
          ''')

c.execute('''
          DROP TABLE Deposit
          ''')

conn.commit()
conn.close()
