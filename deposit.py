from sqlalchemy import Column, Integer, String, DateTime, Float  
from base import Base
import datetime


class Deposit(Base):  
    """ Deposit """

    __tablename__ = "Deposit"  

    id = Column(Integer, primary_key=True)
    account_id = Column(String(250), nullable=False)  
    deposit_date = Column(String(100), nullable=False)  
    trace_id = Column(String(100), nullable=False)  
    date_created = Column(DateTime, nullable=False)
    deposit_amount = Column(Float, nullable=False) 
    account_balance = Column(Float, nullable=False) 

    def __init__(self, account_id, deposit_date, deposit_amount, account_balance,trace_id):

        """ Initializes a Deposit record """
        self.account_id = account_id
        self.deposit_date = deposit_date
        self.date_created = datetime.datetime.now()
        self.deposit_amount = deposit_amount
        self.account_balance = account_balance
        self.trace_id = trace_id


    def to_dict(self):
        """ Dictionary Representation of a Deposit record """
        dict = {}
        dict['id'] = self.id
        dict['account_id'] = self.account_id
        dict['deposit_amount'] = self.deposit_amount
        dict['deposit_date'] = self.deposit_date
        dict['date_created'] = self.date_created
        dict['account_balance'] = self.account_balance
        dict['trace_id'] = self.trace_id
        return dict
