import mysql.connector
import yaml


with open('app_conf.yaml', 'r') as f:
    app_config = yaml.safe_load(f.read())

user = app_config['datastore']['user']
password = app_config['datastore']['password']
hostname = app_config['datastore']['hostname']
port = app_config['datastore']['port']
db = app_config['datastore']['db']


db_conn = mysql.connector.connect(
    host=hostname,
    user=user,
    password=password,
    database=db,
    port=port,
    auth_plugin='mysql_native_password'
)

db_cursor = db_conn.cursor()


withdrawal_table_sql = '''
    CREATE TABLE IF NOT EXISTS Withdrawal
    (id INT NOT NULL AUTO_INCREMENT,
     account_id REAL NOT NULL,
     fee REAL NOT NULL,
     withdrawal_amount REAL NOT NULL,
     withdrawal_date VARCHAR(100) NOT NULL,
     date_created VARCHAR(100) NOT NULL,
     trace_id VARCHAR(100) NOT NULL,
     PRIMARY KEY (id))
'''

deposit_table_sql = '''
    CREATE TABLE IF NOT EXISTS Deposit
    (id INT NOT NULL AUTO_INCREMENT,
     account_id REAL NOT NULL,
     deposit_amount REAL NOT NULL,
     account_balance REAL NOT NULL,
     deposit_date VARCHAR(100) NOT NULL,
     date_created VARCHAR(100) NOT NULL,
     trace_id VARCHAR(100) NOT NULL,
     PRIMARY KEY (id))
'''

db_cursor.execute(withdrawal_table_sql)
db_cursor.execute(deposit_table_sql)


db_conn.commit()
db_conn.close()
