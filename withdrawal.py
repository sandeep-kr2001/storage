from sqlalchemy import Column, Integer, String, DateTime, Float  
from base import Base
import datetime


class Withdrawal(Base):  
    """ Withdrawal """

    __tablename__ = "Withdrawal"  

    id = Column(Integer, primary_key=True)
    account_id = Column(Integer, nullable=False)
    fee = Column(Float, nullable=False)  
    withdrawal_amount = Column(Float, nullable=False) 
    withdrawal_date = Column(String(100), nullable=False)  
    date_created = Column(DateTime, nullable=False)
    trace_id = Column(String(100), nullable=False)  

    def __init__(self,account_id, fee, withdrawal_amount, withdrawal_date,trace_id):
        """ Initializes a Withdrawal record """
        self.account_id = account_id
        self.fee = fee
        self.withdrawal_amount = withdrawal_amount
        self.withdrawal_date = withdrawal_date
        self.date_created = datetime.datetime.now()
        self.trace_id = trace_id

    def to_dict(self):
        """ Dictionary Representation of a Withdrawal record """
        dict = {}
        dict['id'] = self.id
        dict['account_id'] = self.account_id
        dict['fee'] = self.fee
        dict['withdrawal_amount'] = self.withdrawal_amount
        dict['withdrawal_date'] = self.withdrawal_date
        dict['date_created'] = self.date_created
        dict['trace_id'] = self.trace_id

        return dict
